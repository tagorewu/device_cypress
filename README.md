# Cypress Arm Cortex-m4 Coretex-m0plus 异构双核Soc移植l0

## 1. 简介
Cypress PSoC 6 MCUs 配置简要说明：

- 32-bit dual core (Arm Cortex-M4 and Arm Cortex M0+) CPU subsystem

- Flash: 1MB; RAM: 288KB

- BLE 5.1

  此工程主要目的是将ohos l0 移植至此异构双核芯片。m0 裸奔， m4 运行 liteos_m。

## 2. 当前进度

​	移植主要参考 openharmony device/qemu/arm_mps2_an386 工程。

​	当前已将 liteos_m + uart shell 移植至m4 核。m0 编译暂未纳入。

​	目前测试方法是使用 PSoC Creater 上预编译好的cm0p.elf，预下载至芯片内，主要用来启动m4。然后再将此工程编译出来的 cm4.elf 下载至芯片，即可运行 m4核。

​	BSP 依赖 cypress PDL 库。以及使用PSoC Creater 原理图工具自动生成外设基础配置代码，在Generated_Source 目录下。

### 	TODO:

 	将m0 的代码编译纳入hb build 构建系统。使用elf tool 自动拼接双核代码。
